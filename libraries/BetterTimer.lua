function BetterTimer(delay, callback) 

	local bTimer = {

		delay2 = delay,
		callback2 = callback,
		
		running = false,
		timer = nil,
		
	}
	
	function bTimer.isRunning() 
		return bTimer.running
	end

	local function call()
		bTimer.running = false
		bTimer.timer = nil
		bTimer.callback2()
	end
	
	function bTimer.start()
		if bTimer.running then return false end
		bTimer.running = true

		bTimer.timer = timer.delay(bTimer.delay2, false, call)
		
		return true
	end

	function bTimer.cancel() 
		if not bTimer.running then return false end
		timer.cancel(bTimer.timer)
		bTimer.running = false
		return true
	end

	return bTimer
end